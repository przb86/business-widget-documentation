<!-- @format -->

<!-- Improved compatibility of Nach oben link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->

<a name="readme-top"></a>

<!-- PROJECT LOGO -->
<br />
<div align="center">

  <h3 align="center">Entwicklung einer App zur Unterstützung/Beratung von Unternehmen bei der digitalen Transformation 💻</h3>

  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="img/home.png" alt="Logo" width="300">
  </a>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Inhaltsverzeichnis</summary>
  <ol>
    <li>
      <a href="#about-the-project">Über das Projekt</a>
    </li>
    <li>
      <a href="#built-with">Über das Projekt</a>
    </li>
    <li>
      <a href="#start-app">Start der Anwendung</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#backend">Backend</a></li>
        <li><a href="#frontend">Frontend</a></li>
      </ul>
    </li>
    <li><a href="#datamodell">Datenmodell</a></li>
    <li><a href="#diagramme">Diagramme</a></li>
    <li><a href="#mockups">Mockups</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#tutorials">Tutorials</a></li>
    <li><a href="#contact">Kontakt</a></li>
    <li><a href="#links">Links</a></li>

  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## Über das Projekt

### Entwicklung einer App zur Unterstützung/Beratung von Unternehmen bei der digitalen Transformation

<a name="about-the-project"></a>

Um Unternehmen bei der digitalen Transformation zu unterstützen bzw. sie zunächst für das Vorhaben zu sensibilisieren, ist es wichtig, den Unternehmen die Möglichkeit zu geben, die Potenziale der digitalen Transformation zu erkennen 📈.

Dazu wurde in dieser Arbeit eine Applikation entwickelt 📱. Die Applikation ist in der Lage, über eine Umfrage den aktuellen Stand und die Ziele eines Unternehmens zu erfassen und anhand eines Reifegradmodells deutlich zu machen. Anhand dessen werden konkrete Handlungsempfehlungen abgeleitet, welche die jeweiligen Unternehmen bei der individuellen, digitalen Neuausrichtung unterstützen sollen. Diese Applikation dient als Grundlage für ein einheitliches Beratungskonzept zur digitalen Transformation von mittelständischen Unternehmen, welche bei Bedarf in der Praxis gegebenenfalls durch eine individuelle Beratung ergänzt werden kann 🎯.

Die genaue Vorgehensweise des Projektes, Datenmodell, architektonische Entscheidung sind in einer Ausarbeitung `WIP_digitale_Tansformation.pdf` zu finden.

<p align="right">(<a href="#readme-top">Nach oben</a>)</p>

## Gebaut mit 👨🏻‍💻

<a name="built-with"></a>

Das Projekt verwendet ein Client-Server-Modell der Kommunikation, dementsprechend wurden bei der Entwicklung verschiedene Frontend und Backend Technologien benutzt. Außerdem wurden in dem Projekt auch sämtliche Tools für Design und Organisation des Projektes verwendet. Hier sind ein Paar Beispiele:

- [![React][react.js]][react-url]
- [![Bootstrap][bootstrap.com]][bootstrap-url]
- [![strapi][strapi.io]][strapi-url]
- [![node.js][nodejs.org]][nodejs-url]
- [![sqlite][sqlite.org]][sqlite-url]
- [![figma][figma.com]][figma-url]

<p align="right">(<a href="#readme-top">Nach oben</a>)</p>

<!-- GETTING STARTED -->

## Start der Anwendung

<a name="start-app"></a>

Um die Anwendung zu starten, müssen Sie Backend- und Frontend-Projekte installieren, konfigurieren und starten.

### Voraussetzungen

<a name="prerequisites"></a>

Zum Starten des Projektes sollen Sie eine folgende Softwarepakete installieren:

- npm

```sh
npm install npm@latest -g
```

Unten finden Sie ein Beispiel dafür, wie Sie das Projekt **lokal** installieren und einrichten können.

### Installation Backend

<a name="backend"></a>

1. Klonen Sie das Backend-Repository
   ```sh
   git clone git@git.thm.de:przb86/business-widget-backend.git
   ```
2. Installieren Sie NPM-Pakete
   ```sh
   npm install
   ```
3. Installieren Sie Dokumentation
   ```sh
   npm run strapi install documentation
   ```
4. Generieren Sie ein Sendgrid API-Schlüssel (Sendgrid wird in diesem Projekt für Mail-Versand eingesetzt) wie folgt [https://docs.sendgrid.com/ui/account-and-settings/api-keys](https://docs.sendgrid.com/ui/account-and-settings/api-keys)
5. Schreiben Sie Sendgrid API-Schlüssel in `.env` Datei
   ```js
   SENDGRID_API_KEY = YOUR_TOKEN;
   ```
6. Passen Sie in der `src/api/client/content-types/client/lifecycles.js` Datei `from` und `replyTo` Felder an
7. Startend Sie das Projekt mit folgendem Befehl
   ```sh
     npm run develop
   ```
8. Öffnen Sie die Homepage des Backends unter [http://localhost:1337](http://localhost:1337), loggen sich dort ein und erstellen ein Frontend-API Schlüssel ([https://docs.strapi.io/user-docs/latest/settings/managing-global-settings.html#managing-api-tokens](https://docs.strapi.io/user-docs/latest/settings/managing-global-settings.html#managing-api-tokens)).

<p align="right">(<a href="#readme-top">Nach oben</a>)</p>

<!-- USAGE EXAMPLES -->

### Installation Frontend

<a name="frontend"></a>

1. Klonen Sie das Frontend-Repository
   ```sh
   git clone git@git.thm.de:przb86/business-widget.git
   ```
2. Installieren Sie NPM-Pakete
   ```sh
   npm install
   ```
3. Schreiben Sie Ihr Backend-API-Schlüssel in `.env` Datei. Nach Bedarf können Sie Ihr Backend-Endpoint definieren:
   ```js
   REACT_APP_API_KEY=YOUR_KEY
   REACT_APP_API_URL=http://localhost:1337/api/
   ```
4. Startend Sie Frontend mit folgendem Befehl
   ```sh
     npm start
   ```
5. Öffnen Sie die App über [http://localhost:3000/](http://localhost:3000/)

<!-- ROADMAP -->

## Datenmodell

<a name="datamodell"></a>
Zwecks **Kompatibilität** der Anwendung mit verschiedenen Reifegradmodellen wurde ein **generisches** Datenbankmodell entwickelt. In der Praxis bedeutet das, dass Reifegradmodelle für verschiedenen Bereiche in der Anwendung eingespielt werden könnten. Da das Datenbankmodell generisch ist, spielt es für die Anwendung keine Rolle, wenn die Anzahl von Stufen oder Themengebieten geändert wird.

Das Datenbankmodell bildet **alle Sachverhalte** und Inhalte des o.g. Kriterienkataloges, wie Themengebiete, Aspekte, Kriterien, Stufen und Handlungsempfehlungen ab. Zugleich wer- den dort auch funktionale Daten wie Sessions, Zustand und Einstellungen der Anwendung, Kundendaten, Ergebnisse und Inhalte von Views abgespeichert. Das generische Daten- bankmodell ermöglicht Konfiguration und Änderung der Inhalte von einzelnen Views, ohne jegliche Eingriffe in Frontend durchzuführen. Dafür wird ausschließlich Backend verwendet.

<p float="left">
  <img src="img/datenmodell.png" width="500" />
</p>

## Mockups

<a name="mockups"></a>

Zum öffnen von Figma-Mockups brauchen Sie eine Desktop oder Web Version von Figma. Diese finden Sie auf folgender Seite [www.figma.com](https://www.figma.com). Importieren Sie `design/mockups.fig` Datei in Figma, um Mockups des Projektes anzuschauen.

Andere Möglichkeit die Mockups zu öffnen ist über folgenden Link:
[www.figma.com/file/business-widget/](https://www.figma.com/file/18HSx47Pzvxsqa5pxwNV9A/Masterprojekt?node-id=0%3A1&t=4k9AgFe2fxcNnzTB-1)

Eine Übersicht über alle Mockups finden Sie in einer `design/mockups.pdf` PDF-Datei.

<p float="left">
  <img src="screenshots/home.png" width="260" />
  <img src="screenshots/choose.png" width="260" />
</p>

<p float="left">
  <img src="screenshots/question.png" width="260" />
  <img src="screenshots/anmelden.png" width="260" />
</p>

<p float="left">
  <img src="screenshots/error.png" width="260" />
  <img src="screenshots/modal.png" width="260" />
</p>

<p float="left">
  <img src="screenshots/activate.png" width="260" />
  <img src="screenshots/registration.png" width="260" />
</p>

<p float="left">
  <img src="screenshots/radar-chart.png" width="260" />
  <img src="screenshots/radar-chart-2.png" width="260" />
</p>

<p float="left">
  <img src="screenshots/radar-2.png" width="260" />
  <img src="screenshots/mail.png" width="260" />
</p>

<p float="left">
  <img src="screenshots/plan-1.png" width="260" />
  <img src="screenshots/plan-2.png" width="260" />
</p>

<p float="left">
  <img src="screenshots/plan-description.png" width="260" />
</p>

## Diagramme

<a name="diagramme"></a>

Zum öffnen von Draw.io-Diagrammen gehen Sie auf [www.draw.io](https://app.diagrams.net/) und importieren Sie die Datei unter `graphs/masterprojekt.drawio`.

## Roadmap 🚗

<a name="roadmap"></a>

- [x] Entwicklung von MVC
- [x] Refactoring
- [x] Bugfixes 🪲
- [x] Dokumentation 📃
- [ ] Mehrsprachigkeit 㮾
- [ ] Umsetzung von mehreren Beratungskonzepten in einem CMS
- [ ] User-Ranking 📊
- [ ] Variable Gewichtung von Kriterien

<p align="right">(<a href="#readme-top">Nach oben</a>)</p>

<!-- CONTACT -->

## Tutorials

<a name="tutorials"></a>

Video-Tutorials für dieses Projekt sind unter folgendem Link zu finden:

[https://www.youtube.com/playlist?list=PLbdTOTQmWaB6Ai_jPRZizb6pZfXitDiHE]

Die Playlist **Business Widget App Documentation** besteht aus folgenden Videos:

1. [Vorführung der Anwendung](https://www.youtube.com/watch?v=EA9SeNQ9yy4&list=PLbdTOTQmWaB6Ai_jPRZizb6pZfXitDiHE&index=2)

2. [Sendgrid API Key Erstellung](https://www.youtube.com/watch?v=a3-UmGQ_5kQ&list=PLbdTOTQmWaB6Ai_jPRZizb6pZfXitDiHE&index=3)

3. [Backend Installation Teil 1](https://www.youtube.com/watch?v=gC6I0xeCd48&list=PLbdTOTQmWaB6Ai_jPRZizb6pZfXitDiHE&index=4)

4. [Backend Installation Teil 2](https://www.youtube.com/watch?v=wOfA5CH_uZ8&list=PLbdTOTQmWaB6Ai_jPRZizb6pZfXitDiHE&index=4)

5. [Frontend Token Generierung](https://www.youtube.com/watch?v=P3k-jDmE3m4&list=PLbdTOTQmWaB6Ai_jPRZizb6pZfXitDiHE&index=5)

6. [Frontend Installation](https://www.youtube.com/watch?v=lQcLH3fCHW0&list=PLbdTOTQmWaB6Ai_jPRZizb6pZfXitDiHE&index=6)

7. [Informationen zu Views editieren](https://www.youtube.com/watch?v=4WKNDcjBA-M&list=PLbdTOTQmWaB6Ai_jPRZizb6pZfXitDiHE&index=7)

8. [Themengebiete und Unterthemen hinzufügen](https://www.youtube.com/watch?v=tCHvXGTXGsc&list=PLbdTOTQmWaB6Ai_jPRZizb6pZfXitDiHE&index=8)

<a href="https://www.youtube.com/playlist?list=PLbdTOTQmWaB6Ai_jPRZizb6pZfXitDiHE">
  <img src="img/tutorials.png" alt="Logo" width="500">
</a>

<p align="right">(<a href="#readme-top">Nach oben</a>)</p>


## Kontakt

<a name="contact"></a>

Pavlo Rozbytskyi - [pavlo.rozbytskyi@mni.thm.de](mailto:pavlo.rozbytskyi@mni.thm.de)

Erik Damm - [erik.damm@mnd.thm.de](mailto:erik.damm@mnd.thm.de)

<p align="right">(<a href="#readme-top">Nach oben</a>)</p>

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

## Wichtige Links

<a name="links"></a>

Backend [https://git.thm.de/przb86/business-widget-backend](https://git.thm.de/przb86/business-widget-backend)

Frontend [https://git.thm.de/przb86/business-widget](https://git.thm.de/przb86/business-widget)

Strapi [https://strapi.io](https://strapi.io)

Node.js [https://nodejs.org/en/](https://nodejs.org/en/)

Sendgrid [https://docs.sendgrid.com/](https://docs.sendgrid.com/)

[sqlite.org]: https://img.shields.io/badge/-%20SQLite-%23003B57?logo=SQLite&style=for-the-badge
[figma.com]: https://img.shields.io/badge/-%20Figma-black?logo=Figma&style=for-the-badge
[nodejs.org]: https://img.shields.io/badge/-%20Node.js-green?logo=Node.js&style=for-the-badge
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-frontend-url]: https://git.thm.de/przb86/business-widget-fri/-/issues
[issues-backend-url]: https://git.thm.de/przb86/business-widget-backend/-/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[react.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[react-url]: https://reactjs.org/
[bootstrap.com]: https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white
[bootstrap-url]: https://getbootstrap.com
[jquery-url]: https://jquery.com
[strapi-url]: https://strapi.io/
[strapi.io]: https://img.shields.io/badge/-%20Strapi-%232F2E8B?logo=Strapi&style=for-the-badge
[nodejs-url]: https://www.nodejs.org/
[sendgrid-url]: https://www.sendgrid.com/
[sqlite-url]: https://www.sqlite.org/index.html
[figma-url]: https://www.figma.com/
